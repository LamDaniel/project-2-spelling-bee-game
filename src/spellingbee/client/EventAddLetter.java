//Daniel Lam ID: 1932789
package spellingbee.client;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

/**
 * EventAddLetter is a class that adds an event to every letter button.
 * When clicked, it adds a letter to the textfield of the word to submit.
 * @author Daniel Lam
 */
public class EventAddLetter implements EventHandler<ActionEvent>
{
	//Variables
	private String letter;
	private TextField word;
	
	/**
	 * Constructor for EventAddLetter
	 * @param letter takes the letter assigned
	 * @param word takes the textfield of word to submit
	 */
	public EventAddLetter(String letter, TextField word) 
	{
		this.letter = letter;
		this.word = word;
	}
	
	/**
	 * handle() is an overwritten method that adds the letter of the button to the textfield
	 */
	@Override
	public void handle(ActionEvent e)
	{
		String newWord = word.getText() + letter;
		word.setText(newWord);
	}
}