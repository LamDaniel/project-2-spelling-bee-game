//Daniel Lam ID: 1932789
package spellingbee.client;

import javafx.application.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.control.TabPane.TabClosingPolicy;


/**
 * SpellingBeeClient class is the class that will display the GUI to the user.
 * It contains the tabPane which holds both the GameTab and ScoreTab and represents its contents respectively.
 * @author Daniel Lam
 */
public class SpellingBeeClient extends Application {
	/**
	 * start() method sets up the GUI
	 * @param Stage stage
	 */
	public void start(Stage stage) {
		
		//Initializing objects
		Client client = new Client();
		Group root = new Group(); 

		/**
		 * Scene is associated with container, dimensions and background color
		 */
		Color backgroundColor = Color.web("#1e1d1f",1.0);
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(backgroundColor);

		/**
		 * Tabs
		 */
		TabPane tabPane = new TabPane();
		tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
		tabPane.setMinWidth(5000);
		tabPane.setTabMinWidth(50);
		
		//GameTab
		GameTab gameTab = new GameTab(client);
		
		//ScoreTab
		ScoreTab scoreTab = new ScoreTab(client);
		
		/**
		 * Event Listener for any change on the score
		 * @author Grigor Mihaylov
		 */
		TextField scoreField = gameTab.getScoreField();
		
		scoreField.textProperty().addListener(new ChangeListener<String>() {
				@Override
				public void changed(ObservableValue<? extends String> arg0, String arg1, String arg2) {
					scoreTab.refresh();
				}
		});

		//Add both tabs into tabPane
		tabPane.getTabs().addAll(gameTab, scoreTab); 
		
		/**
		 * Displaying GUI
		 */
		root.getChildren().add(tabPane);
		
		/**
		 * Associate scene to stage and then show
		 */
		stage.setTitle("Spelling Bee"); 
		stage.setScene(scene); 
		stage.show(); 
	}
	
	/**
	 * Main method to run application
	 * @param args
	 */
	public static void main(String[] args) {
		Application.launch(args);
	}
}    