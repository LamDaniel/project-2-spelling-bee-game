//Grigor Mihaylov ID:1937997
package spellingbee.client;

import javafx.scene.control.*;
import javafx.scene.layout.*;

/**
 * ScoreTab is a class that extends Tab and it displays the GUI for the Score tab.
 * This is where the player can view his total score and view the brackets that he has accomplished.
 * @author Grigor Mihaylov
 */
public class ScoreTab extends Tab {
	
	//Variables
	private Client client;
	int[] brackets;
	TextField[] bracketTitles;
	TextField scoreValue;
	
	/**
	 * Parameterized Constructor for ScoreTab
	 * @param client client object
	 */
	public ScoreTab(Client client) {
		super("Score");
		this.client = client;
		this.brackets = getBrackets();
		this.bracketTitles = new TextField[5];
		this.setContent(getLayout()); 
		this.refresh();
	}
	
	/**
	 * refresh() method refreshes the score of SpellingBeeClient class whenever it's invoked.
	 * If it detects the score has passed a certain bracket, it changes text color to indicate that.
	 */
	public void refresh() {
		int score = Integer.parseInt(client.sendAndWaitMessage("getScore"));
		this.scoreValue.setText(String.valueOf(score));
			
		for (int i = 0; i < this.brackets.length; i++) {
			if (score >= this.brackets[i]) {
				this.bracketTitles[i].setStyle("-fx-text-fill: black;");
			}
			else {
				this.bracketTitles[i].setStyle("-fx-text-fill: gray;");
			}
		}
	}
	
	/**
	 * getBrackets() is method that returns an array of the brackets
	 * @return tempArray array of brackets converted to int
	 */
	private int[] getBrackets() {
		String bracketsToString = this.client.sendAndWaitMessage("getBrackets");
		String[] bracketsArr = bracketsToString.split(";");
		
		int[] tempArray = new int[5];	
		tempArray[0] = Integer.parseInt(bracketsArr[0]);
		tempArray[1] = Integer.parseInt(bracketsArr[1]);
		tempArray[2] = Integer.parseInt(bracketsArr[2]);
		tempArray[3] = Integer.parseInt(bracketsArr[3]);
		tempArray[4] = Integer.parseInt(bracketsArr[4]);
		
		return tempArray;
	}
	
	/**
	 * getLayout() returns the layout of the ScoreTab which is a HBox containing all of its textfields and brackets
	 * @return HBox layout of the ScoreTab
	 */
	private HBox getLayout() {
		
		//Containers
		HBox layout = new HBox();
		VBox bracketNames = new VBox();
		VBox bracketValues = new VBox();
	
		//Titles
		TextField queenBeeTf = new TextField("Queen Bee");
		TextField geniusTf = new TextField("Genius");
		TextField amazingTf = new TextField("Amazing");
		TextField goodTf = new TextField("Good");
		TextField startedTf = new TextField("Getting started");
		TextField curScore = new TextField("Current Score");
		queenBeeTf.setEditable(false);
		geniusTf.setEditable(false);
		amazingTf.setEditable(false);
		goodTf.setEditable(false);
		startedTf.setEditable(false);
		curScore.setEditable(false);
		
		//Assigning bracketTitles
		this.bracketTitles[4] = queenBeeTf;
		this.bracketTitles[3] = geniusTf;
		this.bracketTitles[2] = amazingTf;
		this.bracketTitles[1] = goodTf;
		this.bracketTitles[0] = startedTf;
			
		//Value of brackets
		TextField queenBeeScore = new TextField("" + brackets[4]);
		TextField geniusScore = new TextField("" + brackets[3]);
		TextField amazingScore = new TextField("" + brackets[2]);
		TextField goodScore = new TextField("" + brackets[1]);
		TextField startedScore = new TextField("" + brackets[0]);
		queenBeeScore.setEditable(false);
		geniusScore.setEditable(false);
		amazingScore.setEditable(false);
		goodScore.setEditable(false);
		startedScore.setEditable(false);
	
		//Total score
		TextField totalScore = new TextField(client.sendAndWaitMessage("getScore"));
		this.scoreValue = totalScore;
		totalScore.setStyle("-fx-text-fill: red;");
		totalScore.setEditable(false);
		
		//Assigning all of the textfields to its respective containers
		bracketValues.getChildren().addAll(queenBeeScore, geniusScore, amazingScore, goodScore, startedScore, totalScore);
		bracketNames.getChildren().addAll(queenBeeTf, geniusTf, amazingTf, goodTf, startedTf, curScore);
		layout.getChildren().addAll(bracketNames, bracketValues);
			
		return layout;
	}
}
