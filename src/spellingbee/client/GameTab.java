//Daniel Lam ID: 1932789
package spellingbee.client;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * GameTab is a class that extends Tab and it displays the GUI for the Game tab.
 * This is where the user plays the game and guesses words with the letter combinations.
 * @author Daniel Lam
 */
public class GameTab extends Tab 
{
	//Variables
	private Client client;
	private TextField scoreField;
	
	/**
	 * Constructor for GameTab. Sets up the name of the tab and invokes function to set up its layout.
	 * @param client
	 */
	public GameTab(Client client) 
	{
		super("Game");
		this.client = client;
		this.setContent(getLayout());
	}
	
	/**
	 * getLayout() returns the layout of the tab which is a VBox that contains all of the letter buttons, word textfields, submit button, score and message textfields.
	 * It also stores the EventHandlers for anonymous functions and invokes the EventAddLetter for EventHandlers when clicking on a letter button.
	 * @return overall VBox that contains the GUI of gameTab
	 */
	public TextField getScoreField() {
		return scoreField;
	}
	
	/**
	 * getLayout() is a method that sets up the GUI with its buttons, textfields and event listeners
	 * @return overall VBox that contains the elements of the GUI
	 */
	private VBox getLayout()
	{
		/**
		 * Containers
		 */
		VBox overall = new VBox();
		HBox tab = new HBox();
		HBox lettersContainer = new HBox();
		HBox wordDisplay = new HBox();
		HBox wordEventsContainer = new HBox();
		HBox scoreAndMsgContainer = new HBox();
		
		/**
		 * wordField to display the word
		 */
		TextField wordField = new TextField();
		
		/**
		 * Score and Message textfields
		 */
		TextField message = new TextField();
		TextField score = new TextField();
		message.setEditable(false);
		score.setEditable(false);
		scoreField = score;
		
		/**
		 * Buttons of letters
		 */
		String letters = client.sendAndWaitMessage("getLetters");
		
		Button[] buttonArr = new Button[7];
		for (int i = 0; i < buttonArr.length; i++)
		{
			buttonArr[i] = new Button("" + letters.charAt(i));
			buttonArr[i].setOnAction(new EventAddLetter(buttonArr[i].getText(), wordField));
		}
		//Set centerLetter to have different style than other buttons
		buttonArr[3].setStyle("-fx-text-fill: red;");
		
		/**
		 * Submit button to submit the word to server.
		 * Contains anonymous function to add event handler to submit.
		 */
		Button submit = new Button("Submit");
		submit.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e)
			{
				//Checks if input of wordField isn't empty
				String word = "";
				if (wordField.getText().equals(""))
				{
					word = " ";  
				}
				else 
				{
					word = wordField.getText().toLowerCase();
				}
		
				String result = client.sendAndWaitMessage("wordCheck;" + word);
				int semiColonIndex = result.indexOf(';');
				message.setText(result.substring(0, semiColonIndex));
				
				String gameScore = client.sendAndWaitMessage("getScore");
				score.setText(gameScore);
				wordField.setText("");			
			}
		});
		
		/**
		 * deleteAll is a button that resets the wordField back to empty.
		 * Contains anonymous function to add event handler that delete all.
		 */
		Button deleteAll = new Button("Delete all");
		deleteAll.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e)
			{
				wordField.setText("");
			}
		});
		
		/**
		 * clearLastLetter is a button that removes the last letter from the wordField.
		 * Contains anonymous function to add event handler that clears last letter
		 */
		Button clearLastLetter = new Button("Clear Last Letter");
		clearLastLetter.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e)
			{
				String word = wordField.getText();
				wordField.setText(word.substring(0, word.length()-1));
			}
		});
		
		/**
		 * Adding contents to containers
		 */
		lettersContainer.getChildren().addAll(buttonArr);
		wordDisplay.getChildren().add(wordField);
		wordEventsContainer.getChildren().addAll(submit, deleteAll, clearLastLetter);
		scoreAndMsgContainer.getChildren().addAll(message, score);
		
		overall.getChildren().addAll(tab, lettersContainer, wordDisplay, wordEventsContainer, scoreAndMsgContainer);
		
		return overall;
	}
}