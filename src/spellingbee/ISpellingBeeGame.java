//Daniel Lam (1932789) and Grigor Mihaylov (1937997)
package spellingbee;

/**
 * Interface for all SpellingBeeGame classes (required methods)
 * @author Daniel Lam
 * @author Grigor Mihaylov
 */
public interface ISpellingBeeGame {
	public int getPointsForWord(String attempt);
	public String getMessage(String attempt);
	public String getAllLetters();
	public char getCenterLetter();
	public int getScore();
	public int[] getBrackets();
}