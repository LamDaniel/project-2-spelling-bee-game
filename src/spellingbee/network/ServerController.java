//Daniel Lam (1932789) and Grigor Mihaylov (1937997)
package spellingbee.network;

import spellingbee.*;

/**
 * This class will run on the server side and is used to connect the server code to the backend business code.
 * This class is where the "protocol" will be defined.
 * @author Grigor Mihaylov
 */
public class ServerController {
	// This is the interface you will be creating!
	// Since we are storing the object as an interface, you can use any type of ISpellingBeeGame object.
	// This means you can work with either the SpellingBeeGame OR SimpleSpellingBeeGame objects and can
	// seamlessly change between the two.
	
	private ISpellingBeeGame spellingBee = new SpellingBeeGame(); 
	
	//SimpleSpellingBeeGame: private ISpellingBeeGame spellingBee = new SimpleSpellingBeeGame();

	/**
	 * Action is the method where the protocol translation takes place.
	 * This method is called every single time that the client sends a request to the server.
	 * It takes as input a String which is the request coming from the client. 
	 * It then does some actions on the server (using the ISpellingBeeGame object)
	 * and returns a String representing the message to send to the client
	 * @param inputLine The String from the client
	 * @return The String to return to the client
	 */
	public String action(String inputLine) {
		if (inputLine.equals("getBrackets")) {
			int[] brackets = spellingBee.getBrackets();
			String bracketsToString = brackets[0] + ";" + brackets[1] + ";" + brackets[2] + ";" + brackets[3] + ";" + brackets[4]; 
			return bracketsToString;
		}
		if (inputLine.equals("getScore")) {
			return String.valueOf(spellingBee.getScore());
		}
		if (inputLine.equals("getLetters")) {
			return spellingBee.getAllLetters();
		}
		
		String[] components = inputLine.split(";");
		String request = components[0];
		
		if (request.equals("wordCheck")) {
			String word = components[1];
			String msg = spellingBee.getMessage(word);
			int points;
			
			if (msg.equals("good")) {
				points = spellingBee.getPointsForWord(word);
			}
			else {
				points = 0;
			}
			return msg + ";" + points;
		}
		return "Invalid Request Form Client";
	}
}