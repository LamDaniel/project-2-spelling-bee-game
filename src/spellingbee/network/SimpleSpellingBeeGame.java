//Daniel Lam ID: 1932789
package spellingbee.network;

import java.util.ArrayList;
import java.util.Arrays;

import spellingbee.ISpellingBeeGame;

/**
 * SimpleSpellingBeeGame is a simplified version of the SpellingBeeGame.
 * It still contains many of the same methods.
 * @author Daniel Lam
 */
public class SimpleSpellingBeeGame implements ISpellingBeeGame
{
	//Variables
	private String letters;
	private char centerL;
	private int score;
	private ArrayList<String> wordsGuessed;
	private ArrayList<String> possibleWords;
	
	/**
	 * Default constructor for SimpleSpellingBeeGame
	 */
	public SimpleSpellingBeeGame()
	{
		this.letters = "sletzrv";
		this.centerL = 't';
		this.wordsGuessed = new ArrayList<String>();
		this.possibleWords = new ArrayList<String>(Arrays.asList("letters","settler","street","steel","trees","tester","let","letter","settlers"));
	}
	
	//Interface methods (including obligatory getters)
	/**
	 * getPointsForWord() is a method that takes in a String and returns the number of points awarded based on its length and if it's a pangram.
	 * @param attempt String that the user submitted in wordDisplay
	 * @return number of points
	 */
	@Override
	public int getPointsForWord(String attempt) {
		int attemptLength = attempt.length();
		String letters = getAllLetters();
		if (attemptLength == 4)
		{
			addPoints(1);
			return 1;
		}
		else if (attemptLength > 4)
		{
			for (int i = 0; i < letters.length(); i ++)
			{
				if (!(attempt.contains("" + letters.charAt(i))))
				{
					addPoints(attemptLength);
					return attemptLength;
				}
			}
			addPoints(attemptLength + letters.length());
			return attemptLength + letters.length();
		}
		else
		{
			return 0;
		}
	}

	/**
	 * getMessage() is method that takes in the String of wordDisplay and returns a message based on a few conditions.
	 * If it passes those checks, the method returns good else it returns the appropriate error message
	 * @param attempt String that the user submitted in wordDisplay
	 * @return correct message to server
	 */
	@Override
	public String getMessage(String attempt) 
	{
		if (wordsGuessed.contains(attempt))
		{
			return "already found";
		}
		if (!(getPossibleWords().contains(attempt)))
		{
			return "not a word";
		}
		if (attempt.length() <= 3)
		{
			return "word too short";
		}
		if (!(attempt.contains("" + centerL)))
		{
			return "middle letter missing";
		}
		addGuessedWord(attempt);
		return "good";
	}
	
	/**
	 * getAllLetters() is a method that returns all the letters of the letter combination.
	 * @return letters letter from letter combination
	 */
	@Override
	public String getAllLetters() 
	{
		return letters;
	}
	
	/**
	 * getCenterLetter() is a method that returns the center letter of the letter combination
	 * @return centerL char that represents center letter
	 */
	@Override
	public char getCenterLetter() 
	{
		return centerL;
	}
	
	/**
	 * getScore() is a method that returns the total score of the user.
	 * @return score total score
	 */
	@Override
	public int getScore() 
	{
		return score;
	}
	
	/**
	 * getBrackets() is a method that returns the amounts of each bracket that the player in this case. (in this case, its hardcoded examples)
	 * @return int[] of brackets
	 */
	@Override
	public int[] getBrackets()
	{
		return new int[] {10,20,30,40,50};
	}
	
	//Additional methods
	/**
	 * getPossibleWords() returns the list of possible words from english.txt.
	 * @return ArrayList<String> possibleWords list of English words
	 */
	private ArrayList<String> getPossibleWords()
	{
		return possibleWords;
	}
	
	/**
	 * addPoints is a method that adds points from guessing a word into score
	 * @param points points gained from word
	 */
	private void addPoints(int points)
	{
		score = score + points;
	}
	
	/**
	 * addGuessedWord() is a method that adds a guessed English word into the ArrayList<String> wordsGuessed when guessed from the user
	 * @param guessedWord word that has been guessed
	 */
	private void addGuessedWord(String guessedWord)
	{
		wordsGuessed.add(guessedWord);
	}
}