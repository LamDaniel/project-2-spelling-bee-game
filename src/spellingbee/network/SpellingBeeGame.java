//Grigor Mihaylov ID:1937997
package spellingbee.network;

import spellingbee.ISpellingBeeGame;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.nio.file.Path;
import java.io.*;

/**
 * SpellingBeeGame class is a class that sets up the Spelling Bee game.
 * It fetches a random letter combination and a list of all of the English words and awards points to the user if they find words that matches with the letter combination.
 * @author Grigor Mihaylov
 */
public class SpellingBeeGame implements ISpellingBeeGame {
	
	//Variables
	private String letters;
	private char centerL;
	private ArrayList<String> wordsGuessed;
	private static ArrayList<String> possibleWords = createFromFile("files\\english.txt");
	private int score;
	
	/**
	 * Default constructor for SpellingBeeGame
	 */
	public SpellingBeeGame() {
		ArrayList<String> letterCombinations = createFromFile("files\\letterCombinations.txt");
		Random rand = new Random();
		String letters = letterCombinations.get(rand.nextInt(letterCombinations.size()));
		this.letters = letters;
		this.centerL = letters.charAt(3);
		this.wordsGuessed = new ArrayList<String>();
		this.score = 0;
	}
	
	//Interface methods (required methods)
	/**
	 * getBrackets() calculates a set of brackets based on the maximum points it can gain from the letter combination and returns it as an array.
	 * @return int[] array of brackets
	 */
	@Override
	public int[] getBrackets() {
		int maxPoints = 0;
		
		//Check max points of every word that works with letter combinations and center letter
		for (String word : possibleWords) {
			if (word.length() > 3 && usesOnlyLetters(word, this.letters) && word.contains("" + this.centerL)) {
				maxPoints += this.getPointsForWord(word);
			}
		}
		this.score = 0;
		
		//Assigning brackets
		int[] brackets = new int[5];
		brackets[0] = maxPoints/4;
		brackets[1] = maxPoints/2;
		brackets[2] = (maxPoints*3)/4;
		brackets[3] = (maxPoints*9)/10;
		brackets[4] = maxPoints;
		return brackets;
	}
	
	/**
	 * getPointsForWord() takes in an attempt from the user and returns a set of points based on its length and game rules.
	 * @return points number of points awarded
	 */
	@Override
	public int getPointsForWord(String attempt) {
		int points = 0;
		if (attempt.length() == 4) {
			points++;
		}
		else {
			points += attempt.length();
		}
		
		boolean isPanagram = true;
		for (int i = 0; i < this.letters.length(); i++) {
			if(!attempt.contains("" + this.letters.charAt(i))) {
				isPanagram = false;
			}
		}
		if (isPanagram) {
			points+=7;
		}
		
		this.score += points;
		return points;
	}

	/**
	 * getMessage() takes in an attempt from the user and returns a message based on the Spelling Bee game rules.
	 * If it's a valid word, it returns good else it returns appropriate error message.
	 * @return String appropriate message
	 */
	@Override
	public String getMessage(String attempt) {
		if (!possibleWords.contains(attempt)) {
			return "not a word";
		}
		if (attempt.length() <= 3) {
			return "word too short";
		}
		if (!usesOnlyLetters(attempt, this.letters)) {
			return "invalid word";
		}
		if (!attempt.contains("" + this.centerL)) {
			return "middle letter missing";
		}
		if (this.wordsGuessed.contains(attempt)) {
			return "already found";
		}
		this.wordsGuessed.add(attempt);
		return "good";
	}
	
	/**
	 * getAllLetters() is a method that returns all the letters of the letter combinations.
	 * @return letters letters of letter combinations
	 */
	@Override
	public String getAllLetters() {
		return letters;
	}

	/**
	 * getCenterLetter() is a method that returns the center letter.
	 * @return centerL center letter
	 */
	@Override
	public char getCenterLetter() {
		return centerL;
	}

	/**
	 * getScore() is a method that returns the total score of the user.
	 * @return score total score
	 */
	@Override
	public int getScore() {
		return score;
	}

	//Additional methods (helper methods)
	/**
	 * usesOnlyLetters is a method that checks if the word is only made of the target letters
	 * @param word word to check
	 * @param letters target letters
	 * @return boolean true = only letters, false = bad input
	 */
	private static boolean usesOnlyLetters(String word, String letters) {
		for (int i = 0; i<word.length(); i++) {
			if (!letters.contains("" + word.charAt(i))) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * createFromFile() is a method that takes a path which leads to a file and returns a list containing all the elements of the file
	 * @param path
	 * @return ArrayList<String> list that contains all the elements
	 */
	private static ArrayList<String> createFromFile(String path) {
		ArrayList<String> output = null;
		File file = new File(path);
		Path absolutePath = Paths.get(file.getAbsolutePath());
		try {
			List<String> lines =  Files.readAllLines(absolutePath);
			output = (ArrayList<String>) lines;
		}
		catch (IOException e) {
			System.err.println("No such file\n");
		}
		return output;
	}
}